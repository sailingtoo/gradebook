<?php 
$course_id = $_GET['course_id'];

$page_title = "Course Details";
$page_descript = "Page lists all the assignments for the specific student";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "<script src=../js/student.js></script>";


include("../includes/config.inc.php");
include("../includes/dataaccess/CourseDataAccess.inc.php");
include("../includes/dataaccess/AssignmentDataAccess.inc.php");
require_once("../includes/header.inc.php");

if($_SERVER['REQUEST_METHOD'] == "GET"){

	$user_id = $_SESSION['user_id'];

	$course_da = new CourseDataAccess(get_link());
	$assignment_da = new AssignmentDataAccess(get_link());

	$course = $course_da->get_courses_by_user_id($course_id);
	$all_assignments = $assignment_da->get_all_assignments_by_user_id_and_course_id($user_id, $course_id);

	// var_dump($course);
	// var_dump($all_assignments);

	$courseInfo = $course_da->fill_in_data_for_course_details_index($course);
	$all_assignments_info = $assignment_da->fill_in_data_for_assignment_details_index_students($all_assignments);

}

?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navagation</h3><br>
				<a href="index.php">Student Home</a>
				<!-- see grades and assigment  -->
			</div>
		</div>
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
				<center><h3>Course Details</h3></center><br>
				<table>
					<?php echo "$courseInfo"; ?>
				</table>
				<br>
				<table id="tableId" class="courseTable">
					<?php echo "$all_assignments_info"; ?>
				</table>
			</div>
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>