<?php
////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$page_title = "Student Homepage";
$page_descript = "Page lists the courses under the specific student";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='/css/form.css'> <link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "<script src='../js/student.js'></script>";

include("../includes/config.inc.php");
include("../includes/dataaccess/CourseDataAccess.inc.php");
require_once("../includes/header.inc.php");

if($_SERVER['REQUEST_METHOD'] == "GET"){

	$user_id = $_SESSION['user_id'];
	$course_da = new CourseDataAccess(get_link());
	$courses = $course_da->get_student_classes_by_userId($user_id);
	// var_dump($course);

	$coursesInfo = $course_da->fill_in_data_for_course_details_index($courses);
}

?>
	<div id="container-content">

		<div id="content-left" class="aside left-main">
			<div class="content content-border middle">
				<!-- up coming assignemnts not turned in.
				Maybe some filter so user can change how far ahead or close they want to see assignments. 
				-->
				<h3>Navagation</h3><br>
				<a href="index.php">Student Home</a>

				<!-- java function that create a div with assignment name, due date, and link-->
			</div>
		</div>

		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
			<!-- Shows student classes with class profressor name inside.-->
				<center><h3>All Courses</h3></center><br>
				<table id="tableId" class="courseTable">
					<?php echo($coursesInfo) ?>
				</table>
			</div>
		</div>
	</div>
<?php 
	require_once("../includes/footer.inc.php");
?>