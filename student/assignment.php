<?php 
$assignment_id = $_GET['assignment_id'];

$page_title = "Submit Assignment";
$page_descript = "Page that lets students submit a file to an assignment";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='/css/form.css'>";
//page specific javascript
$page_js = "";


include("../includes/config.inc.php");
include("../includes/dataaccess/AssignmentDataAccess.inc.php");
require_once("../includes/dataaccess/FileDataAccess.inc.php");
include("../includes/dataaccess/SubmitDataAccess.inc.php");
require_once("../includes/header.inc.php");

$link = get_link();
$submit_da = new SubmitDataAccess($link);
$assignmentInfo = "";
$assignment = array();

$submit = array();
$submit['submit_id'] = 0;
$submit['assignment_id'] = 0;
$submit['user_id'] = 0;
$submit['file_id'] = 0;
$submit['submit_feedback'] = "";
$submit['submit_date'] = "";
$submit['submit_student_score'] = "";

// make sure to include the trailing slash.
$upload_dir = "../uploaded-files/";

if($_SERVER['REQUEST_METHOD'] == "GET"){

	$assignment_da = new AssignmentDataAccess(get_link());
	$assignment = $assignment_da->get_assignment_by_assignments_id($assignment_id);

	// var_dump($assignment);

	$assignmentInfo = $assignment_da->fill_in_data_for_assignment($assignment);

} else if ($_SERVER['REQUEST_METHOD'] === "POST"){

	$submit['submit_id'] = $_POST['txtSubmitId'];
	$submit['assignment_id'] = $_POST['txtAssignmentId'];
	$submit['user_id'] = $_SESSION['user_id'];
	$submit['submit_date'] = date('Y-m-d');
	$submit['submit_feedback'] = "";
	$submit['submit_student_grade'] = "";

	// die($_SESSION['user_id']);

	//process the file upload
	if(isset($_FILES['file_upload']) && !empty($_FILES['file_upload']['name'])){
		
		// we'll allow these types of files to be uploaded
		//NIALL I NEVER ASKED YOU ABOUT THIS
		$allowed_file_types = array('image/pjpeg','image/jpeg','image/JPG','image/X-PNG','image/PNG','image/png','image/x-png');
		
		// instantiate our data access class			
		$file_da = new FileDataAccess($link);

		// set up the array that we pass into the insert_file() method
		$file = array(
			'file_name' => $_FILES['file_upload']['name'], 
			'file_extension' => $file_da->get_file_extension($_FILES['file_upload']['name']), 
			'file_size' =>$_FILES['file_upload']['size'], 
			'file_uploaded_by_id' => 1, // NOTE WE'LL CHANGE THIS LATER 
			'file_uploaded_date' => date("Y-m-d")
		);

		if($file = $file_da->insert_file($file)){
			
			// now that we've inserted a row for the file in the files table
			// lets upload the file...				
			try{
				$file_da->upload_file("file_upload", $allowed_file_types, $upload_dir);
			}catch(Exception $e){
				$error_messages['file_upload'] = $e->getMessage();
			}

			// Now that the image has been uploaded, let's rename it so that it
			// is named by the file id, rather than the original name
			$new_file_name = $file['file_id'] . "." . $file['file_extension'];
			
			if(rename($upload_dir . $file['file_name'], $upload_dir . $new_file_name) === FALSE){
				$error_messages['file_upload'] = "Unable to rename file after uploading";
			}

			// update the $user array so that it inserts/updates the new file name in the users table
			$submit['file_id'] = $new_file_name;
		
		}else{
			$error_messages['file_upload'] = "Unable to insert file into db.";
		}

	}// end of file uploading process

	if(empty($error_messages)){

		if($submit['submit_id'] > 0){
			$submit_da->update_submit($submit);
		} else{
			$submit_da->insert_submit($submit);
		}

		header('Location: /student/index.php');
	}

}// end of the post request function
?>

	<div id="container-content">
		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navagation</h3><br>
				<a href="index.php">Student Home</a>
			</div>
		</div>
		<div id="content-center" class="center-user">
			<div class="content content-border">
				<?php echo($assignmentInfo); ?>

				<div class="form-item form-item-sm">
					<div class="label">Assignment File:</div>
					<div class="input"><a href="/uploaded-files/<?php echo($assignment[0]['file_id']); ?>">download</a></div>
				</div>

				<form method="POST" action="<?php echo($_SERVER['PHP_SELF'])?>" enctype="multipart/form-data">

					<!-- Form Title  -->
					<div class="form-item form-title">
						<h2>Submit Assignment</h2>
					</div>

					<!-- Submit Id -->
					<div style="display: none;">
						<div class="input">
							<span></span>
							<textarea name="txtSubmitId" maxlength="200" class="input-text"></textarea>
						</div>
					</div>
					
					<!-- Assignment Id -->
					<div style="display: none;">
						<div class="input">
							<span></span>
							<input name="txtAssignmentId" maxlength="200" class="input-text" value="<?php echo($assignment_id) ?>"></input>
						</div>
					</div>

					<!-- Uploading File -->
					<div class="form-item form-item-sm">
						<div class="label">Upload File:</div>
						<div class="input">
							<input type="file" id="file_upload" name="file_upload" />
							<?php echo(isset($error_messages['file_upload']) ? $error_messages['file_upload'] : "" ); ?>
						</div>
					</div>

					<!-- Submit Button -->
					<div class="form-item form-item-sm">
						<div class="label">&nbsp;</div>
						<div class="input">
							<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
						</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>