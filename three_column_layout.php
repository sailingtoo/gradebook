<?php
////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$page_title = "TITLE HERE";
$page_descript = "DESCRIPTION HERE";
//page specific css links
$page_links = "";
//page specific javascript
$page_js = "";

require_once("includes/header.inc.php");
?>

	<div id="container-content">

		<div id="content-left" class="aside left-main">
			<div class="content">
				<!-- insert content -->
			</div>
		</div>

		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-main">
			<div class="content content-border">
				<!-- insert content -->
			</div>
		</div>
		
		<div id="content-right" class="aside right-main">
			<div class="content">
				<!-- insert content -->
			</div>
		</div>
	</div>

<?php 
	require_once("includes/footer.inc.php");
?>