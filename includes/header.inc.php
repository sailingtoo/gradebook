<!DOCTYPE html>
<html>
<head>
	<!-- Page title -->
	<title><?php echo($page_title); ?></title>
	<!-- Page description -->
	<meta name="description" content="<?php echo($page_descript) ?>">
	<meta charset="utf-8"/>
	<!-- style sheets -->
	<link rel="stylesheet" type="text/css" href="/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
	
	<!-- Commented out favicon links below -->
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.png">
	<link rel="icon" href="/favicon.png" type="image/x-icon">
	<?php echo($page_links); ?>

	<!-- <script type="text/javascript" src="/js/main.js"></script> -->
	<?php echo($page_js); ?>
	<?php 
		if(isset($_SESSION['role'])){
			if($_SESSION['role'] == 3){
				$editUser = '/create_teacher.php';
			} else if ($_SESSION['role'] == 2){
				$editUser = '/create_user.php';
			} else {
				header('Location: /logout.php');
			}
		}
	?>
</head>
<body>
<div id="main-container" class="clearfix">
	<div id="header">
		<div class="header-content">
			<!-- icon for the gradebook -->
			<!-- <img src="#" class="img-left img-right" alt="icon"> -->
			<div id="title">
				<h1 class="title">Grade Book</h1>
			</div>
			<?php 
			if(isset($dontShowUserMenu)){
				
			} else {
				echo ('<div id="menu-button">
							<ul>
								<li>' . $_SESSION['user_first_name'] . '</li>
								<li><a href="' . $editUser . '">Edit Account</a></li>
								<li><a href="/includes/logout.php">Log Out</a></li>
							</ul>
						</div>
				');
			}
			?>
		</div>
	</div>