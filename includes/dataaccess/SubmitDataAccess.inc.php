<?php

class SubmitDataAccess {
	
	private $link;

	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}

	function insert_submit($submit){

		// prevent SQL injection
		$submit['assignment_id'] = mysqli_real_escape_string($this->link, $submit['assignment_id']);
		$submit['user_id'] = mysqli_real_escape_string($this->link, $submit['user_id']);
		$submit['submit_date'] = mysqli_real_escape_string($this->link, $submit['submit_date']);
		$submit['submit_feedback'] = mysqli_real_escape_string($this->link, $submit['submit_feedback']);
		$submit['submit_student_score'] = mysqli_real_escape_string($this->link, $submit['submit_student_score']);
		$submit['file_id'] = mysqli_real_escape_string($this->link, $submit['file_id']);

		if($submit['submit_feedback'] == ""){
			$qStr = "INSERT INTO submit (
					assignment_id,
					user_id,
					submit_date,
					file_id
				) VALUES (
					'{$submit['assignment_id']}',
					'{$submit['user_id']}',
					'{$submit['submit_date']}',
					'{$submit['file_id']}'
				)";
		} else {
			$qStr = "INSERT INTO assignments (
					assignment_id,
					user_id,
					submit_date,
					file_id,
					submit_feedback,
					submit_student_score
				) VALUES (
					'{$submit['assignment_id']}',
					'{$submit['user_id']}',
					'{$submit['submit_date']}',
					'{$submit['file_id']}',
					'{$submit['submit_feedback']}',
					'{$submit['submit_student_score']}'
				)";
		}
		
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the user id that was assigned by the data base
			$submit['submit_id'] = mysqli_insert_id($this->link);
			// then return the assignment
			return $submit;
		}else{
			$this->handle_error("unable to insert submit");
		}

		return false;
	}


	function update_submit($submit){

		// prevent SQL injection
		$submit['assignment_id'] = mysqli_real_escape_string($this->link, $submit['assignment_id']);
		$submit['user_id'] = mysqli_real_escape_string($this->link, $submit['user_id']);
		$submit['submit_date'] = mysqli_real_escape_string($this->link, $submit['submit_date']);
		$submit['submit_feedback'] = mysqli_real_escape_string($this->link, $submit['submit_feedback']);
		$submit['submit_student_score'] = mysqli_real_escape_string($this->link, $submit['submit_student_score']);
		$submit['file_id'] = mysqli_real_escape_string($this->link, $submit['file_id']);


		$qStr = "UPDATE submit SET
					assignment_id = '{$submit['assignment_id']}',
					user_id = '{$submit['user_id']}',
					submit_date = '{$submit['submit_date']}',
					submit_feedback = '{$submit['submit_feedback']}',
					submit_student_grade = '{$submit['submit_student_score']}',
					file_id = '{$submit['file_id']}'

				WHERE submit_id = " . $submit['submit_id'];
					
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $submit;
		}else{
			$this->handle_error("unable to update submit");
		}

		return false;
	}


	// $rowcount = mysqli_num_rows($result);
 	function get_all_submitted_assignments_by_assignment_id($assignment_id){

		$qStr = "SELECT 
				submit.submit_id, submit.assignment_id, submit.user_id, submit.file_id, submit.submit_date, submit.submit_feedback, submit.submit_student_score, 
				user.user_first_name, user.user_last_name, user.user_email,
				assignments.assignment_name, assignments.assignment_due_date, assignments.assignment_desc
				FROM `submit` 
				INNER JOIN user ON user.user_id = submit.user_id
				INNER JOIN assignments ON assignments.assignment_id = submit.assignment_id
				WHERE submit.submit_student_score = ' ' 
				AND submit.submit_feedback = ' ' 
				AND submit.assignment_id = " . mysqli_real_escape_string($this->link, $assignment_id);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_submitted_assignments = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$assignments = array();
 			$assignments['submit_id'] = htmlentities($row['submit_id']);
 			$assignments['assignment_id'] = htmlentities($row['assignment_id']);
 			$assignments['user_id'] = htmlentities($row['user_id']);
 			$assignments['file_id'] = htmlentities($row['file_id']);
 			$assignments['submit_date'] = htmlentities($row['submit_date']);
 			$assignments['submit_feedback'] = htmlentities($row['submit_feedback']);
 			$assignments['submit_student_score'] = htmlentities($row['submit_student_score']);
 			$assignments['user_first_name'] = htmlentities($row['user_first_name']);
 			$assignments['user_last_name'] = htmlentities($row['user_last_name']);
 			$assignments['user_email'] = htmlentities($row['user_email']);
 			$assignments['assignment_name'] = htmlentities($row['assignment_name']);
 			$assignments['assignment_due_date'] = htmlentities($row['assignment_due_date']);
 			$assignments['assignment_desc'] = htmlentities($row['assignment_desc']);

 			// add the $course to the $all_users array
 			$all_submitted_assignments[] = $assignments;
 		}

 		return $all_submitted_assignments;
			
 	}

	function fill_in_data_submitted($submitted_assignment){

 		$submitted_assignment_Info = 
 		"<tr>
 			<th><h4>Date Submitted</h4></th>
 			<th><h4>Student</h4></th>
 		</tr>";

		for ($i=0; $i < count($submitted_assignment); $i++) { 

			$submitted_assignment_Info .= 

			"<tr class = 'courseStyles' submittedData = '" . $submitted_assignment[$i]['submit_id'] . "'>" .
				"<td>"	.
					"<div class = 'dueDateTd'>"	.
						$submitted_assignment[$i]['submit_date'] .
					"</div>". 
				"</td>"	.
				"<td>"	.
					"<div class = 'userNameTd'>"	.
						$submitted_assignment[$i]['user_first_name'] . " " . $submitted_assignment[$i]['user_last_name'] .
					"</div>". 
				"</td>"	.
			"</tr>";
		}

		return $submitted_assignment_Info;

 	}

 	function get_submitted_assignment_by_user_id_assignment_id($submit_id){
 		$qStr = "SELECT submit_id, assignment_id, user_id, file_id, submit_date, submit_feedback, submit_student_score
 				FROM submit
 				WHERE submit_id = " . mysqli_real_escape_string($this->link, $submit_id);
 			//die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

 		if($result){

 			$row = mysqli_fetch_assoc($result);

 			$submitted_assignment = array();
 			$submitted_assignment['submit_id'] = htmlentities($row['submit_id']);
 			$submitted_assignment['assignment_id'] = htmlentities($row['assignment_id']);
 			$submitted_assignment['user_id'] = htmlentities($row['user_id']);
 			$submitted_assignment['file_id'] = htmlentities($row['file_id']);
 			$submitted_assignment['submit_date'] = htmlentities($row['submit_date']);
 			$submitted_assignment['submit_feedback'] = htmlentities($row['submit_feedback']);
 			$submitted_assignment['submit_student_score'] = htmlentities($row['submit_student_score']);

 			return $submitted_assignment;

 		}
 	}

 	function update_submitted_assignment($submit){
 		
 		$submit['submit_id'] = mysqli_real_escape_string($this->link, $submit['submit_id']);
 		$submit['submit_feedback'] = mysqli_real_escape_string($this->link, $submit['submit_feedback']);
 		$submit['submit_student_score'] = mysqli_real_escape_string($this->link, $submit['submit_student_score']);

 		$qStr = "UPDATE submit
 				SET submit_feedback = '{$submit['submit_feedback']}', submit_student_score = '{$submit['submit_student_score']}'
 				WHERE submit_id = '{$submit['submit_id']}'";

 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
 		
 		if($result){
 			return $submit;
 		}else{
 			$this->handle_error("unable to update assignment");
 		}

 		return false;
 	}

	/**
	* Handles errors in AssignmentDataAccess
	* 
	* @param array Returns an array of Assignment objects??? Or an array of associative arrays???
	*/
	function handle_error($msg){
		// how do we want to handle this? should we throw an exception
		// and let our custom EXCEPTION handler deal with it?????
		$stack_trace = print_r(debug_backtrace(), true);
		throw new Exception($msg . " - " . $stack_trace);
	}

}
?>