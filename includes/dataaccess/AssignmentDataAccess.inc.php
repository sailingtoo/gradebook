<?php

class AssignmentDataAccess {
	
	private $link;

	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}

	/**
	* Inserts a new user into the Assignments table
	*
	* @param array 		
	*
	* @return array 	Returns an assoc array, along with the new assignment id
	* 					Returns false if something goes wrong.
	*/
	function insert_assignment($assignment){

		// prevent SQL injection
		$assignment['assignment_name'] = mysqli_real_escape_string($this->link, $assignment['assignment_name']);
		$assignment['assignment_score'] = mysqli_real_escape_string($this->link, $assignment['assignment_score']);
		$assignment['assignment_due_date'] = mysqli_real_escape_string($this->link, $assignment['assignment_due_date']);
		$assignment['assignment_desc'] = mysqli_real_escape_string($this->link, $assignment['assignment_desc']);
		$assignment['assignment_active'] = mysqli_real_escape_string($this->link, $assignment['assignment_active']);
		$assignment['course_id'] = mysqli_real_escape_string($this->link, $assignment['course_id']);
		$assignment['file_id'] = mysqli_real_escape_string($this->link, $assignment['file_id']);

		$date = new DateTime($assignment['assignment_due_date']);
		$formatedDate = $date->format("Y-m-d");

		$qStr = "INSERT INTO assignments (
					assignment_name,
					assignment_score,
					assignment_due_date,
					assignment_desc,
					assignment_active,
					course_id,
					file_id
				) VALUES (
					'{$assignment['assignment_name']}',
					'{$assignment['assignment_score']}',
					'{$formatedDate}',
					'{$assignment['assignment_desc']}',
					'{$assignment['assignment_active']}',
					'{$assignment['course_id']}',
					'{$assignment['file_id']}'
				)";
		
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the user id that was assigned by the data base
			$assignment['assignment_id'] = mysqli_insert_id($this->link);
			// then return the assignment
			return $assignment;
		}else{
			$this->handle_error("unable to insert assignment");
		}

		return false;
	}


	/**
	* Updates an existing assignment in the assignments table
	*
	* @param array 		 
	*
	* @return array 	Returns an assoc array with all the user properties
	* 					Returns false if something goes wrong.
	*/
	function update_assignment($assignment){

		// prevent SQL injection
		$assignment['assignment_name'] = mysqli_real_escape_string($this->link, $assignment['assignment_name']);
		$assignment['assignment_score'] = mysqli_real_escape_string($this->link, $assignment['assignment_score']);
		$assignment['assignment_due_date'] = mysqli_real_escape_string($this->link, $assignment['assignment_due_date']);
		$assignment['assignment_desc'] = mysqli_real_escape_string($this->link, $assignment['assignment_desc']);
		$assignment['assignment_active'] = mysqli_real_escape_string($this->link, $assignment['assignment_active']);
		$assignment['course_id'] = mysqli_real_escape_string($this->link, $assignment['course_id']);
		$assignment['file_id'] = mysqli_real_escape_string($this->link, $assignment['file_id']);


		$qStr = "UPDATE assignments SET
					assignment_name = '{$assignment['assignment_name']}',
					assignment_score = '{$assignment['assignment_score']}',
					assignment_due_date = '{$assignment['assignment_due_date']}',
					assignment_discription = '{$assignment['assignment_desc']}',
					assignment_active = '{$assignment['assignment_active']}',
					course_id = '{$assignment['course_id']}',
					file_id = '{$assignment['file_id']}'

				WHERE assignment_id = " . $assignment['assignment_id'];
					
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $assignment;
		}else{
			$this->handle_error("unable to update assignment");
		}

		return false;
	}

	/**
	* Handles errors in AssignmentDataAccess
	* 
	* @param array Returns an array of Assignment objects??? Or an array of associative arrays???
	*/
	function handle_error($msg){
		// how do we want to handle this? should we throw an exception
		// and let our custom EXCEPTION handler deal with it?????
		$stack_trace = print_r(debug_backtrace(), true);
		throw new Exception($msg . " - " . $stack_trace);
	}



	// This belongs in the Assignment Data Access file that jeff is making
 	function get_all_assignments_by_user_id_and_course_id($user_id, $course_id){

		$qStr = "SELECT 
		assignments.assignment_id, assignments.course_id, assignments.file_id, 
		assignments.assignment_desc, assignments.assignment_due_date, assignments.assignment_score, 
		assignments.assignment_name, 
		submit.user_id, submit.submit_student_score, (submit.submit_student_score / assignments.assignment_score) as grade
		FROM assignments 
		LEFT JOIN submit ON submit.assignment_id = assignments.assignment_id 
		AND submit.user_id = " . mysqli_real_escape_string($this->link, $user_id) . "
		WHERE assignments.course_id = " . mysqli_real_escape_string($this->link, $course_id);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_assignments = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$assignments = array();
 			$assignments['assignment_id'] = htmlentities($row['assignment_id']);
 			$assignments['course_id'] = htmlentities($row['course_id']);
 			$assignments['file_id'] = htmlentities($row['file_id']);
 			$assignments['assignment_desc'] = htmlentities($row['assignment_desc']);
 			$assignments['assignment_due_date'] = htmlentities($row['assignment_due_date']);
 			$assignments['assignment_score'] = htmlentities($row['assignment_score']);
 			$assignments['assignment_name'] = htmlentities($row['assignment_name']);
 			$assignments['user_id'] = htmlentities($row['user_id']);
 			$assignments['submit_student_score'] = htmlentities($row['submit_student_score']);
 			$assignments['grade'] = htmlentities($row['grade']);

 			// add the $course to the $all_users array
 			$all_assignments[] = $assignments;
 		}

 		return $all_assignments;		
 	}

	//gets the specific assignment by the id
 	function get_assignment_by_assignments_id($assignment_id){

		$qStr = "SELECT 
		assignment_id, course_id, file_id, assignment_desc, DATE_FORMAT(assignment_due_date, '%m/%d/%Y') as assignment_due_date, assignment_score, assignment_name
		FROM assignments 
		WHERE assignment_id = " . mysqli_real_escape_string($this->link, $assignment_id);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_assignments = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$assignments = array();
 			$assignments['assignment_id'] = htmlentities($row['assignment_id']);
 			$assignments['course_id'] = htmlentities($row['course_id']);
 			$assignments['file_id'] = htmlentities($row['file_id']);
 			$assignments['assignment_desc'] = htmlentities($row['assignment_desc']);
 			$assignments['assignment_due_date'] = htmlentities($row['assignment_due_date']);
 			$assignments['assignment_score'] = htmlentities($row['assignment_score']);
 			$assignments['assignment_name'] = htmlentities($row['assignment_name']);

 			// add the $course to the $all_users array
 			$all_assignments[] = $assignments;
 		}

 		return $all_assignments;
			
 	} 		

 	function get_all_assignments_by_course_id($course_id){

		$qStr = "SELECT 
		assignment_id, course_id, file_id, assignment_desc, assignment_due_date, assignment_score, assignment_name
		FROM assignments 
		WHERE course_id = " . mysqli_real_escape_string($this->link, $course_id);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_assignments = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$assignments = array();
 			$assignments['assignment_id'] = htmlentities($row['assignment_id']);
 			$assignments['course_id'] = htmlentities($row['course_id']);
 			$assignments['file_id'] = htmlentities($row['file_id']);
 			$assignments['assignment_desc'] = htmlentities($row['assignment_desc']);
 			$assignments['assignment_due_date'] = htmlentities($row['assignment_due_date']);
 			$assignments['assignment_score'] = htmlentities($row['assignment_score']);
 			$assignments['assignment_name'] = htmlentities($row['assignment_name']);

 			// add the $course to the $all_users array
 			$all_assignments[] = $assignments;
 		}

 		return $all_assignments;
			
 	}

 	function get_student_grades_on_assignments($user_id, $course_id){

		$qStr = "SELECT 
				assignment_name, assignment_desc, assignment_due_date, assignment_score, course_id, 
				submit.submit_date, submit.submit_student_score 
				FROM assignments JOIN submit ON submit.assignment_id = assignments.assignment_id 
				WHERE submit.user_id = " . mysqli_real_escape_string($this->link, $user_id) . 
				" AND course_id = ". mysqli_real_escape_string($this->link, $course_id);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_assignments = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$assignments = array();
 			$assignments['assignment_name'] = htmlentities($row['assignment_name']);
 			$assignments['assignment_desc'] = htmlentities($row['assignment_desc']);
 			$assignments['assignment_due_date'] = htmlentities($row['assignment_due_date']);
 			$assignments['assignment_score'] = htmlentities($row['assignment_score']);
 			$assignments['course_id'] = htmlentities($row['course_id']);
 			$assignments['submit_date'] = htmlentities($row['submit_date']);
 			$assignments['submit_student_score'] = htmlentities($row['submit_student_score']);

 			// add the $course to the $all_users array
 			$all_assignments[] = $assignments;
 		}

 		return $all_assignments;
			
 	}

 	// 	Fills in the assignments div a comesback as a link
 	function fill_in_data_for_assignments($assignments){

 		$assignmentInfo = "";

		for ($i = 0; $i < count($assignments); $i++) { 

			$assignmentInfo .= "<a href = /student/assignment.php/" . $assignments[$i]['assignment_id'] . ">" .
			" assignment_desc: ". $assignments[$i]['assignment_desc']. 
			", assignment_due_date: " . $assignments[$i]['assignment_due_date'] . 
			", assignment_score: " . $assignments[$i]['assignment_score'] .
			"<a> <br>";
		
		}

		return $assignmentInfo;

 	}

	// 	Fills in the assign details index with a list
 	function fill_in_data_for_assignment($assignment){

 		$assignmentInfo = "";

		for ($i=0; $i < count($assignment); $i++) { 

			$assignmentInfo .=  
			"assignment_id: " 		. $assignment[$i]['assignment_id'] 		.
			", course_id: " 		. $assignment[$i]['course_id'] 		.
			", file_id: " 		. $assignment[$i]['file_id'] 		.
			", assignment_desc: ". $assignment[$i]['assignment_desc']. 
			", assignment_due_date: " 			. $assignment[$i]['assignment_due_date'] . 
			", assignment_score: " 	. $assignment[$i]['assignment_score'];
		
		}

		return $assignmentInfo;

 	}

 	function fill_in_data_for_assignment_details_index_students($assignments){

 		$assignmentsInfo = 
 			"<tr class='middle'>
				<th><h4>Assignment</h4></th>
				<th><h4>Due Date</h4></th>
				<th><h4>Grade Status</h4></th>
			</tr>";

		for ($i=0; $i < count($assignments); $i++) { 

			$turnedIn = "courseStyles";
			$grade = "Not submitted";

			if ($assignments[$i]['user_id'] != "") {
				$turnedIn = "courseStyles turnedIn";
				$grade = "Submited";
				if ($assignments[$i]['grade'] != 0) {
					$turnedIn = "courseStyles turnedIn graded";	
					$grade = $assignments[$i]['grade'];
				}
			}


			$assignmentsInfo .= 
				"<tr class =' " . $turnedIn . "'assignmentData = '" . $assignments[$i]['assignment_id'] . "'>" .
					"<td>" 	.
						"<div class='assignmentNameTd'>"	.
						$assignments[$i]['assignment_name'] .
						"</div>". 
					"</td>"	.
					"<td>"	.
						"<div class='dueDateTd'>"	.
						$assignments[$i]['assignment_due_date'] .
						"</div>". 
					"</td>"	.
					"<td>"	.
						"<div class='descTd'>".
							$grade .
						"</div>". 
					"</td>"	.
				"</tr>";
		}

		return $assignmentsInfo;
 	}

 	function fill_in_data_for_student_grades($assignments){

 		$assignmentsInfo = 
 			"<tr class='middle'>
				<th><h4>Assignment</h4></th>
				<th><h4>Due Date</h4></th>
				<th><h4>Submit Date</h4></th>
				<th><h4>Total Points</h4></th>
				<th><h4>Scored Points</h4></th>
			</tr>";

		for ($i=0; $i < count($assignments); $i++) { 

			$assignmentsInfo .= 
				"<tr class ='courseStyles link' assignmentData = '" . $assignments[$i]['course_id'] . "'>" .
					"<td>" 	.
						"<div class='assignmentNameTd'>"	.
							$assignments[$i]['assignment_name'] .
						"</div>". 
					"</td>"	.
					"<td>"	.
						"<div class='dueDateTd'>" .
							$assignments[$i]['assignment_due_date'] .
						"</div>". 
					"</td>"	.
					"<td>"	.
						"<div class='descTd'>"	.
							$assignments[$i]['submit_date'] .
						"</div>". 
					"</td>"	.
					"<td>"	.
						"<div class='descTd'>"	.
							$assignments[$i]['assignment_score'] .
						"</div>". 
					"</td>"	.
					"<td>"	.
						"<div class ='descTd'>"	.
							$assignments[$i]['submit_student_score'] .
						"</div>". 
					"</td>"	.
				"</tr>";
		}
		return $assignmentsInfo;
 	}

 	function fill_in_data_for_assignment_details_index_teachers($assignments){

 		$assignmentsInfo = 
 			"<tr class='middle'>
				<th><h4>Name</h4></th>
				<th><h4>Description</h4></th>
				<th><h4>Due Date</h4></th>
			</tr>";

		if($assignments){
			for ($i=0; $i < count($assignments); $i++) { 

				$assignmentsInfo .= 
					"<tr class = 'courseStyles' assignmentData = '" . $assignments[$i]['assignment_id'] . "'>" .
						"<td>" 	.
							"<div class = 'assignmentNameTd'>"	.
								$assignments[$i]['assignment_name'] .
							"</div>". 
						"</td>" .
						"<td>"	.
							"<div class = 'descTd'>"	.
								$assignments[$i]['assignment_desc'] .
							"</div>". 
						"</td>"	.
						"<td>"	.
							"<div class = 'dueDateTd'>"	.
								$assignments[$i]['assignment_due_date'] .
							"</div>". 
						"</td>"	.
					"</tr>";
			}
		} else {
			$assignmentsInfo .= "
				<tr>
					<td>No Assignments In Course</td>
					<td>To add an assignment, click the 'Add Assignment' link in the Navagation section.</td>
				</tr>";
		}
		return $assignmentsInfo;
		
 	}
}
?>
