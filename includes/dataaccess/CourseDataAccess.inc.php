<?php

class CourseDataAccess{

	private $link;
	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}

	// 	/**
	// 	* Gets all users
	// 	* 
	// 	* @return array Returns an array of Course objects??? 
	// 	* 				Or an array of associative arrays???
	// 	*/
	function get_all_courses_with_user_id($userId){

		$qStr = "SELECT
 					course_id, user_id,course_title, course_description, num_students
 				FROM course
 				WHERE user_id = " . mysqli_real_escape_string($this->link, $userId);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_courses = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$course = array();
 			$course['course_id'] = htmlentities($row['course_id']);
 			$course['course_title'] = htmlentities($row['course_title']);
 			$course['course_description'] = htmlentities($row['course_description']);
 			$course['num_students'] = htmlentities($row['num_students']);

 			// add the $course to the $all_users array
 			$all_courses[] = $course;
 		}

 		return $all_courses;
			
 	}

	function get_student_classes_by_userId($userId){
		$qStr = "SELECT course.course_id, course.course_title, LEFT( course.course_description, 12) 
				FROM usercourse 
				INNER JOIN course on course.course_id = usercourse.course_id
				WHERE usercourse.user_id =" . mysqli_real_escape_string($this->link, $userId);
		
 		 // die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_courses = array();

 		while($row = mysqli_fetch_assoc($result)){
 			// var_dump($row);
 			// create a $course obj and scrub the data to prevent XSS attacks
 			$course = array();
 			$course['course_id'] = htmlentities($row['course_id']);
 			$course['course_title'] = htmlentities($row['course_title']);
 			$course['course_description'] = htmlentities($row['LEFT( course.course_description, 12)']);

 			// add the $course to the $all_users array
 			$all_courses[] = $course;
 		}

 		return $all_courses;
			
 	}

 	function get_courses_by_user_id($course_id){
		$qStr = "SELECT
 					course_id, user_id,course_title, course_description
 				FROM course
 				WHERE course_id = " . mysqli_real_escape_string($this->link, $course_id);
		
 		// die($qStr);

 		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
 		$all_courses = array();

 		while($row = mysqli_fetch_assoc($result)){

 			// create a $course obj and scrub the data to prevent XSS attacks
 			$course = array();
 			$course['course_id'] = htmlentities($row['course_id']);
 			$course['course_title'] = htmlentities($row['course_title']);
 			$course['course_description'] = htmlentities($row['course_description']);

 			// add the $course to the $all_users array
 			$all_courses[] = $course;
 		}

 		return $all_courses;
			
 	}

	// 	Fills in the Teacher index with a list
 	function fill_in_data_for_course_details_index($courses){

 		$courseInfo = 
 			"<tr class='middle'>
				<th><h4>Course</h4></th>
				<th><h4>Descripton</h4></th>
			</tr>";

		for ($i = 0; $i < count($courses); $i++) { 

			$courseInfo .= 
				"<tr class='courseStyles' courseData ='" . $courses[$i]['course_id'] . "'>" .
					
						"<td>" 	.
							"<div class ='titleTd'>" .
							$courses[$i]['course_title'] .
							"</div>". 
						"<td>"	.
							"<div class = 'descTd'>" .
							$courses[$i]['course_description'] .
							"</div>". 
						"</td>"	.
				"</tr>";
		}

		return $courseInfo;
 	}
 	// 	Fills in the Teacher index with a list
 	function fill_in_data_for_course_details($course){

 		$courseInfo = "";

		for ($i=0; $i < count($course); $i++) { 

			$courseInfo .= 
				"<div class = 'classTd'>" .
					"Class" . 
				"</div>". 
				"<div class = titleTd>"	.
					$course[$i]['course_title'] .
					"</div>". 
				"<div class = descTd>" .
					$course[$i]['course_description'] .
				"</div>";		
		}

		return $courseInfo;

 	}

 	function insert_course($course){
		
		$course['course_title'] = mysqli_real_escape_string($this->link, $course['course_title']);
		$course['course_description'] = mysqli_real_escape_string($this->link, $course['course_description']);
		$course['user_id'] = mysqli_real_escape_string($this->link, $course['user_id']);

		$qStr = "INSERT INTO course(
			course_title,
			course_description,
			user_id
		)VALUES(
			'{$course['course_title']}',
			'{$course['course_description']}',
			'{$course['user_id']}'
		)";

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the course id that was assigned by the data base
			$course['course_id'] = mysqli_insert_id($this->link);
			// then return the course
			return $course;
		}else{
			$this->handle_error("unable to insert course");
		}

		return false;
	}

 	function add_students_to_course($students_added, $course_id){
		

		foreach($students_added as $student){

		$student = mysqli_real_escape_string($this->link, $student);

		$qStr = "INSERT INTO usercourse(
			user_id,
			course_id
		)VALUES(
			'{$student}',
			'{$course_id}'
			
		)";

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		
		}
		
		return false;
	
	}

	/**
	* Handles errors in CourseDataAccess
	* 
	* @param array Returns an array of User objects??? Or an array of associative arrays???
	*/
	function handle_error($msg){
		// how do we want to handle this? should we throw an exception
		// and let our custom EXCEPTION handler deal with it?????
		$stack_trace = print_r(debug_backtrace(), true);
		throw new Exception($msg . " - " . $stack_trace);
	}

	// NOTE: we could make a DataAccess super class that has handle_error()
	// in it. Then we could sub class it and all sub classes could share the
	// same method (less code duplication)

}
?>