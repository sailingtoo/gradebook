<?php
// this is the main configuration file for the website
session_start();
define("ADMIN_ROLE", 1);
define("STANDARD_USER_ROLE", 2);

// detect which environment the code is running in
if($_SERVER['SERVER_NAME'] == "localhost"){
	// DEV ENVIRONMENT SETTINGS
	define("DEBUG_MODE", true);
	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASSWORD", "");
	define("DB_NAME", "gradebook");
	define("SITE_ADMIN_EMAIL", "XXXXXXX");
	define("SITE_DOMAIN", $_SERVER['SERVER_NAME']);
	//define("ROOT_DIR", "/adv-web-dev/php/site-setup-7-file-uploads/");

}else{
	// PRODUCTION SETTINGS
	define("DEBUG_MODE", true); 
	// you may want to set DEBUG_MODE to true when you 
	// are first setting up your live site, but once you get
	// everything working you'd want it off.
	define("DB_HOST", "localhost");
	define("DB_USER", "sailingt_sailing");
	define("DB_PASSWORD", '$eP[!QO$vey[');
	define("DB_NAME", "sailingt_gradebook");
	define("SITE_ADMIN_EMAIL", "sailingtoors@gmail.com");
	define("SITE_DOMAIN", $_SERVER['SERVER_NAME']);
}

// if we are in debug mode then display all errors and set error reporting to all 
if(DEBUG_MODE){
	// turn on error messages
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

// the $link variable will be our connection to the database
$link = null;

function get_link(){

	global $link;
		
	if($link == null){
		
		$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

		if(!$link){
			throw new Exception(mysqli_connect_error()); 
		}
	}

	return $link;
}


// set up custom error handling
require_once('custom_error_handler.inc.php');
