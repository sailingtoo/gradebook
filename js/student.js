window.addEventListener("load", function() {
    var table = document.getElementById("tableId");

    if(table){
        var rows = table.getElementsByTagName("tr");

        for (i = 0; i < rows.length; i++) {
            var currentRow = table.rows[i];
            var createClickHandler = 
                function(row) {
                    return function() { 
                        var course_id = row.getAttribute("coursedata");
                        var assignment_id = row.getAttribute("assignmentdata");

                        var targetURL = "/course_details";
                        var id = "course_id=" + course_id;
                                            
                        // alert("course id: " + course_id);
                        // alert("assignemnt id: " + assignment_id);

                        if (course_id == null) {
                            targetURL = "/assignment";
                            id = "assignment_id=" + assignment_id;
                        }
                        // alert(targetURL + " " + id);
                                            
                        window.location = "/student" + targetURL + ".php?" + id;                                 
                    };
                };
            if(currentRow){
                currentRow.onclick = createClickHandler(currentRow);
            };   
        }
    }
});