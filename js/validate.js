window.addEventListener('load', function(){

	var form = document.querySelector("form");

	if(form == document.querySelector("#user")){
		form.addEventListener('submit', validateUser);
	}else if(form == document.querySelector("#course")){
		form.addEventListener('submit', validateCourse);
	}else if(form == document.querySelector("#assignment")){
		form.addEventListener('submit', validateAssignment);
	}

	function validateUser(evt){

		isValid = true;

		var txtFirstName = form.querySelector("#txtFirstName");
		var txtLastName = form.querySelector("#txtLastName");
		// var txtEmail = form.querySelector("#txtEmail");
		var txtPassword = form.querySelector("#txtPassword");
		var txtConfirmPassword = form.querySelector("#txtConfirmPassword");
		

		var vPassword = form.querySelector("#vPassword");
		var vConfirm = form.querySelector("#vConfirmPassword");
		// var vEmail = form.querySelector("#vEmail");

		if(form.querySelector("#vAdminPassword") && form.querySelector("#txtAdminPassword")){
			var vAdminPassword = form.querySelector("#vAdminPassword");
			var txtAdminPassword = form.querySelector("#txtAdminPassword");

			var vMessages = document.querySelectorAll(".validation");
		
			for(var x=0; x<vMessages.length; x++){
				vMessages[x].style.display = "none";
			}

			if(txtAdminPassword.value != "password"){
				vAdminPassword.style.display = "block";
				isValid = false;
				vAdminPassword.innerHTML = "Enter the correct admin password";
			}
		} else {
			var vMessages = document.querySelectorAll(".validation");
		
			for(var x=0; x<vMessages.length; x++){
				vMessages[x].style.display = "none";
			}
		}

		if(txtFirstName.value == ""){
			form.querySelector("#vFirstName").style.display = "block";
			isValid = false;
		}

		if(txtLastName.value == ""){
			form.querySelector("#vLastName").style.display = "block";
			isValid = false;
		}

		if(txtEmail.value == ""){
			isValid = false;
			vEmail.style.display = "block";
			vEmail.innerHTML = "Enter an email";
		}else if(validateEmail(txtEmail.value) == false){
			isValid = false;
			vEmail.style.display = "block";
			vEmail.innerHTML = "Enter a valid email address";
		}

		if(txtPassword.value == ""){
			vPassword.style.display = "block"
			isValid = false;
		}

		if(txtConfirmPassword.value == ""){
			isValid = false;
			form.querySelector("#vConfirmPassword").style.display = "block";
			isValid = false;
		//check if the password match
		}else if(txtPassword.value !== txtConfirmPassword.value){
			isValid = false;
			 vConfirm.style.display = "block";
			 vConfirm.innerHTML = "Passwords must match";
			 vPassword.style.display = "block";
			 vPassword.innerHTML = "Passwords must match";
		}

		function validateEmail(email) {
        	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        	return re.test(email);
    	}


		if(isValid == false){
			evt.preventDefault();
		}
	}

	function validateCourse(evt){

		isValid = true;

		var vMessages = document.querySelectorAll(".validation");
		for(var x=0; x<vMessages.length; x++){
			vMessages[x].style.display = "none";
		}

		var txtCourseTitle = form.querySelector("#txtCourseTitle");
		var txtCourseDescription = form.querySelector("#txtCourseDescription");

		var vCourseTitle = form.querySelector("#vCourseTitle");
		var vCourseDescription = form.querySelector("#vCourseDescription");

		if(txtCourseTitle.value == ""){
			vCourseTitle.style.display = "block";
			isValid = false;
		}

		if(txtCourseDescription.value == ""){
			vCourseDescription.style.display = "block";
			isValid = false;
		}

		if(isValid == false){
			evt.preventDefault();
		}

	}

	function validateAssignment(evt){

		isValid = true;

		var txtAssignmentName = form.querySelector("#txtAssignmentName");
		var txtAssignmentDueDate = form.querySelector("#txtAssignmentDueDate");
		var txtAssignmentScore = form.querySelector("#txtAssignmentScore");
		var txtAssignmentDesc = form.querySelector("#txtAssignmentDesc");

		var vAssignmentName = form.querySelector("#vAssignmentName");
		var vAssignmentDueDate = form.querySelector("#vAssignmentDueDate");
		var vAssignmentScore = form.querySelector("#vAssignmentScore");
		var vAssignmentDesc = form.querySelector("#vAssignmentDesc");

		var vMessages = document.querySelectorAll(".validation");
		for(var x=0; x<vMessages.length; x++){
			vMessages[x].style.display = "none";
		}

		if(txtAssignmentName.value == ""){
			vAssignmentName.style.display = "block";
			isValid = false;
		}

		if(txtAssignmentScore.value == ""){
			vAssignmentScore.style.display = "block";
			isValid = false;
		}

		if(txtAssignmentDueDate.value == ""){
			vAssignmentDueDate.style.display = "block";
			isValid = false;
		}

		if(txtAssignmentDesc.value == ""){
			vAssignmentDesc.style.display = "block";
			isValid = false;
		}


		if(isValid == false){
			evt.preventDefault();
		}

	}





























});