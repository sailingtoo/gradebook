window.addEventListener("load", function() {
    var table = document.getElementById("tableId");

    if(table){

        var rows = table.getElementsByTagName("tr");

        for (i = 0; i < rows.length; i++) {
            var currentRow = table.rows[i];
            var createClickHandler = 
                function(row) {
                    return function() { 
                        var course_id = row.getAttribute("coursedata");
                        var assignment_id = row.getAttribute("assignmentdata");
                        var submit_id = row.getAttribute("submitteddata");
                        var user_id = row.getAttribute("userdata");

                        var targetURL = "/course_details"
                        var id = "course_id=" + course_id;

                        // alert("course id: " + course_id);
                        // alert("assignemnt id: " + assignment_id);
                        // alert("submit id: " + submit_id);
                        // alert("user_id: " + user_id)

                        if (course_id == null && submit_id == null && user_id == null) {
                            targetURL = "/assignment"
                            id = "assignment_id=" + assignment_id;
                        }

                        if (course_id == null && assignment_id == null && user_id == null) {
                            targetURL = "/grading_form"
                            id = "submit_id=" + submit_id;
                        }

                        if (course_id == null && assignment_id == null && submit_id == null) {
                            targetURL = "/student_grades";
                            splited = user_id.split(" ");
                            id = "user_id=" + splited[0] + "&" + "course_id=" + splited[1];
                        }
                        // alert(targetURL + " " + id);
                        window.location = "/teacher" + targetURL + ".php?" + id;
                    };
                };
            if(currentRow){
                currentRow.onclick = createClickHandler(currentRow);
            };        
        }
    } 
});
