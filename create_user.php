<?php
include("includes/config.inc.php");
require_once("includes/dataaccess/UserDataAccess.inc.php");

//header and stuff
$page_title = "Student Details";
$page_descript = "Create or Edit student";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='css/form.css'>";
//page specific javascript
$page_js = "<script type='text/javascript' src='js/validate.js'></script>";
$dontShowUserMenu = "true";
$btnBack = "index.php";

require_once("includes/header.inc.php");

$link = get_link();
$user_da = new UserDataAccess($link);


// set up an empty user obj/array
$user = array();
$user['user_id'] = 0; // set to 0, this will help us determine if we need to update or insert later
$user['user_first_name'] = "";
$user['user_last_name'] = "";
$user['user_email'] = "";
$user['user_password'] = "";
$user['user_confirm_password'] = "";
$user['user_role'] = 2; //default to being a student user
$user['user_active'] = "yes"; // default to letting the user be 'active'

if ($_SERVER['REQUEST_METHOD'] === 'POST'){
	$user['user_id'] = $_POST['txtId'];
	$user['user_first_name'] = $_POST['txtFirstName'];
	$user['user_last_name'] = $_POST['txtLastName'];
	$user['user_email'] = $_POST['txtEmail'];
	//$user['user_role'] = $_POST['selRole'];
	$user['user_password'] = $_POST['txtPassword'];
	$user['user_confirm_password'] = $_POST['txtConfirmPassword'];
	//$user['user_active'] = $_POST['rgActive'];


	$error_messages = validate_input($user);

	if(empty($error_messages)){
		try{
			if($user['user_id'] > 0){
				// if the user_id is greater than 0, then we are doing an UPDATE
				$encrypt_password = true;
				if($_SESSION['original_password'] == $user['user_password']){
					$encrypt_password = false;
				}
				$user_da->update_user($user, $encrypt_password);
				header('Location: student/index.php');
			}else{
				// if the user_id is NOT greater than 0, then we are doing an INSERT
				$user_da->insert_user($user);
				header('Location: index.php');
			}
			// We are done processing this user, redirect to user list page
		}catch(Exception $e){
			if($e->getMessage() == UserDataAccess::DUPLICATE_USER_ERROR){
				$error_messages['user_email'] = "Please choose a different email address";
			}else{
				
			}
		}
	}
}else{
	if(isset($_SESSION['user_id'])){
		$user_id = $_SESSION['user_id'];
		$user = $user_da->get_user_by_id($user_id);
		$page_title = "Edit Student";
		$btnBack = "/student/index.php";
		$user['user_confirm_password'] = $user['user_password'];
		$_SESSION['original_password'] = $user['user_password'];
		//print_r($user);
	}
}

function validate_input($user){
	
	// we'll popluate an array with all the error message
	// Note that if the form is valid, then this array will be empty
	// and we'll check this to see if we should send the data to the db
	$error_messages = array();

	// first name
	if(empty($user['user_first_name'])){
		$error_messages['user_first_name'] = "You must enter a first name";
	}

	// last name
	if(empty($user['user_last_name'])){
		$error_messages['user_last_name'] = "You must enter a last name";
	}

	// email
	if(empty($user['user_email'])){
		$error_messages['user_email'] = "You must enter an email address";
	}else if(filter_var($user['user_email'], FILTER_VALIDATE_EMAIL) == false){
		$error_messages['user_email'] = "The email entered is not valid";
	}

	// password
	if(empty($user['user_password'])){
		$error_messages['user_password'] = "You must enter a password";
	}else if(empty($user['user_confirm_password'])){
		$error_messages['user_confirm_password'] = "You must confirm your password";
	}else if($user['user_password'] != $user['user_confirm_password']){
		$error_messages['user_confirm_password'] = "The passwords do not match";
	}

	// user role
	// do we need to make sure that the user role is within the valid range of user role ids?
	if($user['user_role'] == 0){
		$error_messages['user_role'] = "Please select a user role";
	}

	//active
	if( ($user['user_active'] == "yes" || $user['user_active'] == "no") == FALSE){
		$error_message['user_active'] = "Active is not valid - I suspect FOWL PLAY!";
		// send email to site admin???
	}

	return $error_messages;
}


?>
	<div id="container-content">
		<div id="content-left" class="aside left-main">
			<div class="content">
				<!-- insert content -->
			</div>
		</div>
		<div id="content-center" class="center-main">
			<div class="content content-border" >
				<form method="POST" id="user">
					<div class="form-item form-title">
						<h2><?php echo($page_title); ?></h2>
					</div>
					<div class="form-item" style="display: none;">
						<input type="text" name="txtId" id="txtId" value="<?php echo($user['user_id']); ?>">
					</div>
					<div class="form-item form-item-sm">
						<div class="label">First Name:</div>
						<div class="input">
							<span id="vFirstName" class="validation">First Name is required</span>
							<input type="text" id="txtFirstName" name="txtFirstName" class="input-text" value="<?php echo($user['user_first_name']); ?>" >
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Last Name:</div>
						<div class="input">
							<span id="vLastName" class="validation">Last Name is required</span>
							<input type="text" id="txtLastName" name="txtLastName" class="input-text" value="<?php echo($user['user_last_name']); ?>" >
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Email:</div>
						<div class="input">
							<span id="vEmail" class="validation">Try a new email.</span>
							<input type="text" id="txtEmail" name="txtEmail" class="input-text" value="<?php echo($user['user_email']); ?>" >
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Password:</div>	
						<div class="input">
							<span id="vPassword" class="validation">Password is required</span>
							<input type="password" id="txtPassword" name="txtPassword" class="input-text" value="<?php echo($user['user_password']); ?>" >
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Confirm Password:</div>	
						<div class="input">
							<span id="vConfirmPassword" class="validation">Password confirm is required</span>
							<input type="password" id="txtConfirmPassword" name="txtConfirmPassword" class="input-text" value="<?php echo($user['user_password']); ?>">
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">&nbsp;</div>
						<div class="input">
							<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
							<a href="<?php echo($btnBack) ?>">Go back.</a>
						</div>	
					</div>
				</form>
			</div>
		</div>
		<div id="content-right" class="aside right-main">
			<div class="content">
				<!-- insert content -->
			</div>
		</div>
	</div>


<?php 
	require_once("includes/footer.inc.php");
?>