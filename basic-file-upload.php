<?php
include("includes/config.inc.php");
require_once("includes/dataaccess/UserDataAccess.inc.php");
require_once("includes/dataaccess/FileDataAccess.inc.php");

//page html///////////////////////////////////////////////
$page_title = "TITLE HERE";
$page_descript = "DESCRIPTION HERE";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='css/form.css'>";
//page specific javascript
$page_js = "";
require_once("includes/header.inc.php");
//////////////////////////////////////////////////////////

$link = get_link();

// the dir to upload user images to
// make sure to include the trailing slash.
$upload_dir = "/uploaded-files/";

if($_SERVER['REQUEST_METHOD'] === 'POST') {
	
	//process the file upload
	if(isset($_FILES['file_upload']) && !empty($_FILES['file_upload']['name'])){
		
		// we'll allow these types of files to be uploaded
		$allowed_file_types = array('image/pjpeg','image/jpeg','image/JPG','image/X-PNG','image/PNG','image/png','image/x-png');
		
		// instantiate our data access class			
		$file_da = new FileDataAccess($link);

		// set up the array that we pass into the insert_file() method
		$file = array(
			'file_name' => $_FILES['file_upload']['name'], 
			'file_extension' => $file_da->get_file_extension($_FILES['file_upload']['name']), 
			'file_size' =>$_FILES['file_upload']['size'], 
			'file_uploaded_by_id' => 1, // NOTE WE'LL CHANGE THIS LATER 
			'file_uploaded_date' => date("Y-m-d")
		);

		if($file = $file_da->insert_file($file)){
			
			// now that we've inserted a row for the file in the files table
			// lets upload the file...				
			try{
				$file_da->upload_file("file_upload", $allowed_file_types, $upload_dir);
			}catch(Exception $e){
				$error_messages['file_upload'] = $e->getMessage();
			}

			// Now that the image has been uploaded, let's rename it so that it
			// is named by the file id, rather than the original name
			$new_file_name = $file['file_id'] . "." . $file['file_extension'];
			
			if(rename($upload_dir . $file['file_name'], $upload_dir . $new_file_name) === FALSE){
				$error_messages['file_upload'] = "Unable to rename file after uploading";
			}

			// update the $user array so that it inserts/updates the new file name in the users table
			// $user['user_image'] = $new_file_name;
		
		}else{
			$error_messages['file_upload'] = "Unable to insert file into db.";
		}

	}// end of file uploading process
}
?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border">
				<!-- insert content -->
			</div>
		</div>

		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border">
				<form method="POST" action="<?php echo($_SERVER['PHP_SELF'])?>" enctype="multipart/form-data">

					<!-- Form Title  -->
					<div class="form-item form-title">
						<h2>Uploading File</h2>
					</div>

					<!-- Uploading File -->
					<div class="form-item form-item-sm">
						<div class="label">Upload File:</div>
						<div class="input">
							<input type="file" id="file_upload" name="file_upload" />
							<?php echo(isset($error_messages['file_upload']) ? $error_messages['file_upload'] : "" ); ?>
						</div>
					</div>

					<!-- Submit Button -->
					<div class="form-item form-item-sm">
						<div class="label">&nbsp;</div>
						<div class="input">
							<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
						</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

<?php 
	require_once("includes/footer.inc.php");
?>