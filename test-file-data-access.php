<?php

	function insert_submit($submit){

		// prevent SQL injection
		$submit['assignment_id'] = mysqli_real_escape_string($this->link, $submit['assignment_id']);
		$submit['user_id'] = mysqli_real_escape_string($this->link, $submit['user_id']);
		$submit['submit_date'] = mysqli_real_escape_string($this->link, $submit['submit_date']);
		$submit['submit_feedback'] = mysqli_real_escape_string($this->link, $submit['submit_feedback']);
		$submit['submit_student_grade'] = mysqli_real_escape_string($this->link, $submit['submit_student_grade']);
		$submit['file_id'] = mysqli_real_escape_string($this->link, $submit['file_id']);

		if($submit['submit_feedback'] == ""){
			$qStr = "INSERT INTO assignments (
					assignment_id,
					user_id,
					submit_date,
					file_id,
					user_id
				) VALUES (
					'{$submit['assignment_id']}',
					'{$submit['user_id']}',
					'{$submit['submit_date']}',
					'{$submit['file_id']}',
					'{$submit['user_id']}'
				)";
		} else {
			$qStr = "INSERT INTO assignments (
					assignment_id,
					user_id,
					submit_date,
					file_id,
					user_id,
					submit_feedback,
					submit_student_grade
				) VALUES (
					'{$submit['assignment_id']}',
					'{$submit['user_id']}',
					'{$submit['submit_date']}',
					'{$submit['file_id']}',
					'{$submit['user_id']}',
					'{$submit['submit_feedback']}',
					'{$submit['submit_student_grade']}'
				)";
		}
		
		// die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the user id that was assigned by the data base
			$submit['submit_id'] = mysqli_insert_id($this->link);
			// then return the assignment
			return $submit;
		}else{
			$this->handle_error("unable to insert submit");
		}

		return false;
	}


	function update_submit($submit){

		// prevent SQL injection
		$submit['assignment_id'] = mysqli_real_escape_string($this->link, $submit['assignment_id']);
		$submit['user_id'] = mysqli_real_escape_string($this->link, $submit['user_id']);
		$submit['submit_date'] = mysqli_real_escape_string($this->link, $submit['submit_date']);
		$submit['submit_feedback'] = mysqli_real_escape_string($this->link, $submit['submit_feedback']);
		$submit['submit_student_grade'] = mysqli_real_escape_string($this->link, $submit['submit_student_grade']);
		$submit['file_id'] = mysqli_real_escape_string($this->link, $submit['file_id']);


		$qStr = "UPDATE submit SET
					assignment_id = '{$submit['assignment_id']}',
					user_id = '{$submit['user_id']}',
					submit_date = '{$submit['submit_date']}',
					submit_feedback = '{$submit['submit_feedback']}',
					submit_student_grade = '{$submit['submit_student_grade']}',
					file_id = '{$submit['file_id']}'

				WHERE submit_id = " . $submit['submit_id'];
					
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $submit;
		}else{
			$this->handle_error("unable to update submit");
		}

		return false;
	}
?>