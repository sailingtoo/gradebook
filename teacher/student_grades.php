<?php
include("../includes/config.inc.php");
require_once("../includes/dataaccess/AssignmentDataAccess.inc.php");
require_once("../includes/dataaccess/UserDataAccess.inc.php");

////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$page_title = "Student Grades";
$page_descript = "This page shows all the student assignments to be graded";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='../css/form.css'> <link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "";

require_once("../includes/header.inc.php");

$link = get_link();
$assignment_da = new AssignmentDataAccess($link);
$user_da = new UserDataAccess($link);


$course_id = $_GET['course_id'];
$user_id = $_GET['user_id'];
$user = $user_da->get_user_by_id($user_id);

// var_dump($user['user_first_name']);

$student_grades = $assignment_da->get_student_grades_on_assignments($user_id, $course_id);

$studentInfo = $assignment_da->fill_in_data_for_student_grades($student_grades);

?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>
		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
				<!-- insert content -->
				<center><h3><?php echo($user['user_first_name'] . " " . $user['user_last_name']); ?>'s Grades</h3></center>
				<table id="tableId">
					<?php echo $studentInfo; ?>
				</table>
			</div>
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>