<?php 

$course_id = $_GET['course_id'];


$page_title = "Course Details";
$page_descript = "This page shows the course details";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "<script src=../js/teacher.js></script>";


include("../includes/config.inc.php");
include("../includes/dataaccess/CourseDataAccess.inc.php");
include("../includes/dataaccess/AssignmentDataAccess.inc.php");
require_once("../includes/header.inc.php");

if($_SERVER['REQUEST_METHOD'] == "GET"){

	$course_da = new CourseDataAccess(get_link());
	$assigment_da = new AssignmentDataAccess(get_link());
	$course = $course_da->get_courses_by_user_id($course_id);
	$all_assigments = $assigment_da->get_all_assignments_by_course_id($course_id);
	// var_dump($course);

	$assigmentsInfo = $assigment_da->fill_in_data_for_assignment_details_index_teachers($all_assigments);
}

$add_students_link = "<a href = /teacher/add_students.php?course_id=" . $course_id.">Add Students</a> 			<br>";
$add_assignment_link = "<a href = /teacher/create_assignment.php?course_id=" . $course_id.">Add Assigment</a> 	<br>";
$grades = "<a href = /teacher/all_students.php?course_id=" . $course_id.">Students & Grades</a> 	<br>";
$_SESSION['course_id'] = $course_id;
?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<?php echo "$add_students_link"; ?><br>
				<?php echo "$add_assignment_link"; ?><br>
				<?php echo "$grades"; ?><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
				<center><h3>All Assignments</h3></center>
				<table id="tableId">
					<?php echo "$assigmentsInfo"; ?>
				</table>
			</div>
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>