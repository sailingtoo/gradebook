<?php
include("../includes/config.inc.php");
require_once("../includes/dataaccess/CourseDataAccess.inc.php");
$page_title = "Create Course";
$page_descript = "Create new course";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='../css/form.css'>
	<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "<script type='text/javascript' src='../js/validate.js'></script>";

require_once("../includes/header.inc.php");

$link = get_link();
$course_da = new CourseDataAccess($link);

$course = array();
$course['course_id'] = 0;
$course['course_title'] = "";
$course['course_description'] = "";
$course['user_id'] = "";

if($_SERVER['REQUEST_METHOD'] === 'POST'){

	$course['course_id'] = $_POST['txtCourseId'];
	$course['course_title'] = $_POST['txtCourseTitle'];
	$course['course_description'] = $_POST['txtCourseDescription'];
	$course['user_id'] = $_SESSION['user_id'];

	$error_messages = validate_input($course);

	if(empty($error_messages)){

		$course_da->insert_course($course);
		header("Location: /teacher/index.php");
	}

}



function validate_input($course){

	$error_messages = array();

	if(empty($course['course_title'])){
		$error_messages['course_title'] = "Course title is required";
	}

	if(empty($course['course_description'])){
		$error_messages['course_description'] = "Course description is required";
	}

		return $error_messages;
}

?>

<div id="container-content">
	<div id="content-left" class="aside left-main">
		<div class="content content-border middle">
			<h3>Navigation</h3><br>
			<a href="index.php">Teacher Home</a><br>
			<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
		</div>
	</div>
	<div id="content-center" class="center-user">
		<div class="content content-border middle">
			<center><h3>Create Course</h3></center>
			<form method="POST" id="course">
				<div class="form-item" style="display: none;">
					<input type="text" name="txtCourseId" id="txtCourseId">
				</div>
				<div class="form-item form-item-sm">
					<div class="label">Course Name:</div>
					<div class="input">
						<span id="vCourseTitle" class="validation">Course Title is required</span>
						<input type="text" id="txtCourseTitle" name="txtCourseTitle" class="input-text" >
					</div>
				</div>
				<div class="form-item form-item-lg">
					<div class="label">Description:</div>
					<div class="input">
						<span id="vCourseDescription" class="validation">A description is required</span>
						<textarea type="text" id="txtCourseDescription" name="txtCourseDescription" class="input-text"></textarea>
					</div>
				</div>
				<div class="form-item form-item-sm">
					<div class="label">&nbsp;</div>
					<div class="input">
						<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
					</div>	
				</div>
			</form>
		</div>
	</div>
</div>

<?php 
	require_once("../includes/footer.inc.php");
?>