<?php
include("../includes/config.inc.php");
require_once("../includes/dataaccess/UserDataAccess.inc.php");

////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$page_title = "All Students grades";
$page_descript = "all students to grades";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='../css/form.css'> 
<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "<script src=../js/teacher.js></script>";

require_once("../includes/header.inc.php");

$link = get_link();
$user_da = new UserDataAccess($link);
$course_id = $_GET['course_id'];
$all_students = $user_da->get_all_students_in_course($course_id);

$studentInfo = $user_da->fill_in_data_for_all_students($all_students, $course_id);

?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>
		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
				<center><h3>Student Grades</h3></center>
				<!-- insert content -->
				<table id="tableId">
					<?php echo $studentInfo; ?>
				</table>
			</div>
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>