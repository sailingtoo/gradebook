<?php
include("../includes/config.inc.php");
require_once("../includes/dataaccess/UserDataAccess.inc.php");
require_once("../includes/dataaccess/FileDataAccess.inc.php");
require_once("../includes/dataaccess/AssignmentDataAccess.inc.php");

//page html///////////////////////////////////////////////

$page_title = "Create Assignment";
$page_descript = "Teacher's creating an assignment for a course.";
//page specific css links
$page_links = "
	<link rel='stylesheet' type='text/css' href='/css/form.css'>
	<link rel='stylesheet' href='/js/jquery-ui-1.12.1.custom/jquery-ui.min.css'>";
//page specific javascript
$page_js = "
	<script src='/js/jquery-2.1.3.js'></script>
	<script src='/js/jquery-ui-1.12.1.custom/jquery-ui.js'></script>
	<script>
  		$(function(){
      		// DATE PICKER
      		$( '#txtAssignmentDueDate' ).datepicker({
        		showOn: 'both'//,
        		//buttonImage: 'images/calendar.gif'
        		// you would need to create this image if you wanted it to appear
        		//buttonImageOnly: true,
        		//buttonText: 'Select date'
      		});
    	});// end of jQuery
  	</script>
  	<script type='text/javascript' src='../js/validate.js'></script>
";

require_once("../includes/header.inc.php");
//////////////////////////////////////////////////////////

$link = get_link();
$assignment_da = new AssignmentDataAccess($link);

$assignment = array();
$assignment['assignment_id'] = 0;
$assignment['assignment_name'] = "";
$assignment['assignment_score'] = "";
$assignment['assignment_due_date'] = "";
$assignment['assignment_desc'] = "";
$assignment['assignment_active'] = "yes";
$assignment['course_id'] = $_SESSION['course_id'];
$assignment['file_id'] = "";
// var_dump($assignment);
// the dir to upload user images to
// make sure to include the trailing slash.
$upload_dir = "../uploaded-files/";

if($_SERVER['REQUEST_METHOD'] === 'POST') {
	$assignment['assignment_id'] = $_POST['txtAssignmentId'];
	$assignment['assignment_name'] = $_POST['txtAssignmentName'];
	$assignment['assignment_score'] = $_POST['txtAssignmentScore'];
	$assignment['assignment_due_date'] = $_POST['txtAssignmentDueDate'];
	$assignment['assignment_desc'] = $_POST['txtAssignmentDesc'];
	// $assignment['course_id'] = $_SESSION['course_id'];
	// $assignment['course_id'] = $course_id;

	$error_messages = validate_input($assignment);

	//process the file upload
	if(isset($_FILES['file_upload']) && !empty($_FILES['file_upload']['name'])){
		
		// we'll allow these types of files to be uploaded
//////////// TODO: Change the allowed file types/////////////////////////////////////
		$allowed_file_types = array('image/pjpeg','image/jpeg','image/JPG','image/X-PNG','image/PNG','image/png','image/x-png');
		
		// instantiate our data access class			
		$file_da = new FileDataAccess($link);

		// set up the array that we pass into the insert_file() method
		$file = array(
			'file_name' => $_FILES['file_upload']['name'], 
			'file_extension' => $file_da->get_file_extension($_FILES['file_upload']['name']), 
			'file_size' =>$_FILES['file_upload']['size'], 
			'file_uploaded_by_id' => 1, // NOTE WE'LL CHANGE THIS LATER 
			'file_uploaded_date' => date("Y-m-d")
		);

		if($file = $file_da->insert_file($file)){
			
			// now that we've inserted a row for the file in the files table
			// lets upload the file...				
			try{
				$file_da->upload_file("file_upload", $allowed_file_types, $upload_dir);
			}catch(Exception $e){
				$error_messages['file_upload'] = $e->getMessage();
			}

			// Now that the image has been uploaded, let's rename it so that it
			// is named by the file id, rather than the original name
			$new_file_name = $file['file_id'] . "." . $file['file_extension'];
			
			if(rename($upload_dir . $file['file_name'], $upload_dir . $new_file_name) === FALSE){
				$error_messages['file_upload'] = "Unable to rename file after uploading";
			}

			// update the $user array so that it inserts/updates the new file name in the users table
			$assignment['file_id'] = $new_file_name;
		
		}else{
			$error_messages['file_upload'] = "Unable to insert file into db.";
		}

	}// end of file uploading process

	if(empty($error_messages)){
		if($assignment['assignment_id'] > 0){
			// if the user_id is greater than 0, then we are doing an UPDATE
			$assignment_da->update_assignment($assignment);
		}else{
			// if the user_id is NOT greater than 0, then we are doing an INSERT
			// var_dump($assignment);
			$assignment_da->insert_assignment($assignment);
		}
		// We are done processing this user, redirect to user list page
		header('Location: index.php');
	}

} //end of post request function

function validate_input($assignment){
	$error_messages = array();

	if(empty($assignment['assignment_name'])){
		$error_messages['assignment_name'] = "You must enter an assignment name";
	}
	if(empty($assignment['assignment_due_date'])){
		$error_messages['assignment_due_date'] = "You must enter a due date.";
	}
	if(empty($assignment['assignment_score'])){
		$error_messages['assignment_score'] = "You must enter a Total Score.";
	}
	if(empty($assignment['assignment_desc'])){
		$error_messages['assignment_desc'] = "You must enter a an assignment description.";
	}

	//active
	if( ($assignment['assignment_active'] == "yes" || $assignment['assignment_active'] == "no") == FALSE){
		$error_message['assignment_active'] = "Active is not valid - I suspect FOWL PLAY!";
		// send email to site admin???
	}

	return $error_messages;
}
?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="<?php echo("/teacher/course_details.php?course_id=$course_id"); ?>">Course Home</a><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>

		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border">
				<form method="POST" action="<?php echo($_SERVER['PHP_SELF'])?>" enctype="multipart/form-data" id="assignment">

					<!-- Form Title  -->
					<div class="form-item form-title">
						<h2>Create Assignment</h2>
					</div>
					<!-- Assignment Id -->
					<div class="form-item" style="display: none;">
						<input type="text" name="txtAssignmentId" id="txtAssignmentId">
					</div>

					<!-- Assignment Name -->
					<div class="form-item">
						<div class="label">Assignment Name:</div>
						<div class="input">
							<span id="vAssignmentName" class="validation">Assignment name is required</span>
							<input type="text" id="txtAssignmentName" name="txtAssignmentName" class="input-text" >
						</div>
					</div>

					<!-- Assignment Score -->
					<div class="form-item">
						<div class="label">Assignment Total Score:</div>
						<div class="input">
							<span id="vAssignmentScore" class="validation">Assignment total score is required</span>
							<input type="text" id="txtAssignmentScore" name="txtAssignmentScore" class="input-text" >
						</div>
					</div>

					<!-- Assignment Due Date -->
					<div class="form-item">
						<div class="label">Assignment Due Date:</div>
						<div class="input">
							<span id="vAssignmentDueDate" class="validation">Assignment due date is required</span>
							<input type="text" id="txtAssignmentDueDate" readonly="true" name="txtAssignmentDueDate" class="input-text" >
						</div>
					</div>

					<!-- Description -->
					<div class="form-item form-item-lg">
						<div class="label">Assignment Description:</div>
						<div class="input">
							<span id="vAssignmentDesc" class="validation">Assignment description is required</span>
							<textarea name="txtAssignmentDesc" id="txtAssignmentDesc" maxlength="200" class="input-text"></textarea>
						</div>
					</div>

					<!-- Uploading File -->
					<div class="form-item form-item-sm">
						<div class="label">Upload File:</div>
						<div class="input">
							<input type="file" id="file_upload" name="file_upload" />
							<?php echo(isset($error_messages['file_upload']) ? $error_messages['file_upload'] : "" ); ?>
					</div>

					<!-- Submit Button -->
					<div class="form-item form-item-sm">
						<div class="label">&nbsp;</div>
						<div class="input">
							<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
						</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>