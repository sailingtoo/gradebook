<?php
include("../includes/config.inc.php");
require_once("../includes/dataaccess/UserDataAccess.inc.php");
require_once("../includes/dataaccess/CourseDataAccess.inc.php");

////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$page_title = "Add Students";
$page_descript = "add students to course";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='../css/form.css'>
<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "";

require_once("../includes/header.inc.php");

$link = get_link();
$user_da = new UserDataAccess($link);
$course_id = $_GET['course_id'];
$all_users = $user_da->get_all_students_not_in_course($course_id);
$course_da = new CourseDataAccess($link);


$students_added = array();
$students_added = "";



//print_r($_GET['course_id']);
//to do dont have students already in course on the list

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	
	if(isset($_POST['students'])){
	$students_added = $_POST['students'];
	}else{
		echo("No students were selected");
	}
	//gets course id from the query string

	$error_messages = validate_input($students_added, $course_id);

	if(empty($error_messages)){
		$course_da->add_students_to_course($students_added, $course_id);
		//print_r($_students_added);
		// We are done processing this user, redirect to course details page
		header("Location: /teacher/course_details.php?course_id=$course_id");
	}
	
}

function validate_input($students_added, $course_id){

	$error_messages = array();

	if(empty($students_added)){
		$error_messages['user_id'] = "Student must be selected";
	}

	if(empty($course_id)){
		$error_messages['course_id'] = "Course id needed";
	}

	return $error_messages;
}

?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="/teacher/create_course.php">Create a course</a><br>
				<a href="<?php echo("/teacher/course_details.php?course_id=$course_id"); ?>">Course Home</a><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>
		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border clearfix middle">
				<center><h3>Add Students</h3></center>
				<!-- insert content -->
				<form method="POST">
					<?php 
						foreach($all_users as $user){
							//value of each checkbox is user id
							echo("<input type=\"checkbox\" name=\"students[]\" value=\"$user[user_id]\" >" . $user['user_first_name'] . " " . $user['user_last_name'] . "<br>" ." Email: " . $user['user_email'] . "<br>");
							echo("<br>");
						}
					?>
					<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
				</form>
			</div>
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>