<?php
include("../includes/config.inc.php");
////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$page_title = "Grading Form";
$page_descript = "Grading form";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='../css/form.css'>
<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "";

require_once("../includes/header.inc.php");
include("../includes/dataaccess/SubmitDataAccess.inc.php");
include("../includes/dataaccess/FileDataAccess.inc.php");

$link = get_link();
$submit_da = new SubmitDataAccess($link);
$file_da = new FileDataAccess($link);

$submited_file_ext = array();

if($_SERVER['REQUEST_METHOD'] == "GET"){

	$submit_id = $_GET['submit_id'];

	$submit = $submit_da->get_submitted_assignment_by_user_id_assignment_id($submit_id);

	$submited_file_ext = $file_da->get_file_by_id($submit['file_id']);
	
	$student_file = $submited_file_ext['file_id'] . "." . $submited_file_ext['file_extension'];
}

if($_SERVER['REQUEST_METHOD'] === "POST"){

	$submit['submit_id'] = $_GET['submit_id'];
	$submit['assignment_id'] = $_POST['txtAssignmentId'];
	$submit['submit_date'] = $_POST['txtSubmitDate']; 
	$submit['submit_feedback'] = $_POST['txtSubmitFeedback']; 
	$submit['submit_student_score'] = $_POST['txtStudentScore']; 

	$error_messages = validate_input($submit);

	if(empty($error_messages)){
		$submit_da->update_submitted_assignment($submit);
		header("Location: assignment.php?assignment_id=$submit[assignment_id]");
		//var_dump($submit['assignment_id']);
	}


}

function validate_input($submit){

	$error_messages = array();

	if(empty($submit['submit_feedback'])){
		$error_messages['submit_feedback'] = "Please enter some feedback";
	}

	if(empty($submit['submit_student_score'])){
		$error_messages['submit_student_score'] = "Please enter a grade";
	}

}


?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>

		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
				<center><h3>Grading Form</h3></center>
				<form method="POST">
					<div class="form-item form-item-sm" style="display: none;">
						<div class="label">Assignemnt Id:</div>
						<div class="input">
							<input type="text" class="input-text" name="txtAssignmentId" value="<?php echo $submit['assignment_id'] ?>">
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Submitted Date:</div>
						<div class="input">
							<input type="text" class="input-text" name="txtSubmitDate" value="<?php echo $submit['submit_date'] ?>">
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Submitted File:</div>
						<div class="input">
							<a href="../uploaded-files/<?php echo $student_file ?>">download</a>
						</div>
					</div>
					<div class="form-item form-item-lg">
						<div class="label">Feedback:</div>
						<div class="input">
							<textarea class="input-text" name="txtSubmitFeedback"><?php echo $submit['submit_feedback'] ?></textarea>
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Student Grade:</div>
						<div class="input">
							<input type="text" class="input-text" name="txtStudentScore" value="<?php echo $submit['submit_student_score'] ?>">
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">&nbsp;</div>
						<div class="input">
							<input type="submit" id="btnSubmit" name="btnSubmit" value="Submit">
						</div>	
					</div>					
				</form>
			</div>
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>