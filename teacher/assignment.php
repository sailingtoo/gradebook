<?php 
$assignment_id = $_GET['assignment_id'];
// echo "$assignment_id";

$page_title = "All Assignments";
$page_descript = "All Assignments";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
//page specific javascript
$page_js = "<script src='../js/teacher.js'></script>";


include("../includes/config.inc.php");
include("../includes/dataaccess/AssignmentDataAccess.inc.php");
include("../includes/dataaccess/UserDataAccess.inc.php");
include("../includes/dataaccess/SubmitDataAccess.inc.php");
require_once("../includes/header.inc.php");

if($_SERVER['REQUEST_METHOD'] == "GET"){

	$assignment_da = new AssignmentDataAccess(get_link());
	$user_da = new UserDataAccess($link);
	$submit_da = new SubmitDataAccess($link);

	$assignment = $assignment_da->get_assignment_by_assignments_id($assignment_id);
	$submitted_assignment = $submit_da->get_all_submitted_assignments_by_assignment_id($assignment_id);

	$assignmentInfo = $assignment_da->fill_in_data_for_assignment_details_index_teachers($assignment);
	
	$studentInfo = $submit_da->fill_in_data_submitted($submitted_assignment);

	// var_dump($submitted_assignment);
}

?>

	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="index.php">Teacher Home</a><br>
				<a href="#" onclick="window.history.back()">Back to Last Visited Page</a>
			</div>
		</div>
		<div id="content-center" class="center-user">
			<div class="content content-border middle">
				<center><h3>Assignment Information</h3></center>
				<table id="tableId1">
					<?php echo "$assignmentInfo"; ?>
				</table>
				<br>
				<br>
				<center><h3>Submitted Assignments for Grading</h3></center>
				<table id="tableId">
					<?php echo "$studentInfo"; ?>
				</table>
			</div>
			
		</div>
		
	</div>

<?php 
	require_once("../includes/footer.inc.php");
?>