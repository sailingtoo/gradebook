<?php
	////This is for showing php errors
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);

	$page_title = " Teacher Homepage";
	$page_descript = "Login to the gradebook";
	//page specific css links
	$page_links = "<link rel='stylesheet' type='text/css' href='/css/user-info.css'>";
	//page specific javascript
	$page_js = "<script src=../js/teacher.js></script>";

	include("../includes/config.inc.php");
	include("../includes/dataaccess/CourseDataAccess.inc.php");
	require_once("../includes/header.inc.php");



	if($_SERVER['REQUEST_METHOD'] == "GET"){

		$user_id = $_SESSION['user_id'];
		$course_da = new CourseDataAccess(get_link());
		$course = $course_da->get_all_courses_with_user_id($user_id);
		// var_dump($course);

		$courseInfo = $course_da->fill_in_data_for_course_details_index($course);

	}
?>
	<div id="container-content">

		<div id="content-left" class="aside">
			<div class="content content-border middle">
				<h3>Navigation</h3><br>
				<a href="/teacher/create_course.php">Create a Course</a><br><br>
			</div>
		</div>
		<div id="content-center" class="center-user">
			<div id="courseArea" class="content content-border middle" onload="<?php $course ?>">

				<center><h3>All Classes</h3></center><br>
				<table id="tableId" class="courseTable">
					<?php echo($courseInfo) ?>
				</table>

			</div>
		</div>
		
	</div>


<?php 
	require_once("../includes/footer.inc.php");
?>