<?php

////This is for showing php errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include("includes/config.inc.php");
include("includes/dataaccess/UserDataAccess.inc.php");

//header php
$page_title = "Homepage";
$page_descript = "Login to the gradebook";
//page specific css links
$page_links = "<link rel='stylesheet' type='text/css' href='/css/form.css'>";
//page specific javascript
$page_js = "";

//setting if the login was valid
$isValid = "";
$dontShowUserMenu = "true";

require_once("includes/header.inc.php");

if($_SERVER['REQUEST_METHOD'] == "POST"){

	$username = $_POST['txtUser'];
	$password = $_POST['txtPassword'];

	$user_da = new UserDataAccess(get_link());
	$user = $user_da->login($username, $password);
	//die(var_dump($user));

	if($user){
		session_regenerate_id();
		$_SESSION['user_first_name'] = $user['user_first_name'];
		$_SESSION['role'] = $user['user_role'];
		$_SESSION['user_id'] = $user['user_id'];
		// var_dump($_SESSION['role']);
		// die();

		//set if statement and change header to another page for different user roles
		if($_SESSION['role'] == 2){
			header("Location: /student/index.php");
		} else {
			header("Location: /teacher/index.php");
		}
		
	} else {
		$isValid = "Email and/or password is invalid";
	}
} else {
	// whenever the user comes to this page, we should destroy any session vars
	// and create a new session id (this is the 'phpsessid' cookie)
	session_destroy();
	// session_regenerate_id();
}

?>

<!-- login php code that runs off of the UserDataAccess.inc.php code -->


	<div id="container-content">
		<div id="content-left" class="aside left-main">
			<div class="content">
				<!-- insert content -->
			</div>
		</div>
		<!-- change this divs class to center-user for user pages -->
		<div id="content-center" class="center-main">
			<div class="content content-border">
				<!-- action = index.php -->
				<form method="POST" action="index.php">
					<div class="form-item form-title">
						<h2>Login</h2>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Email:</div>
						<div class="input">
							<input type="text" id="txtUser" name="txtUser" class="input-text">
						</div>
					</div>
					<div class="form-item form-item-sm">
						<div class="label">Password:</div>	
						<div class="input">
							<input type="password" id="txtPassword" name="txtPassword" class="input-text">
						</div>
					</div>
					<!-- change this style and add it to the main style sheet -->
					<div style="text-align: center; padding-bottom: 10px;">
						<div><?php echo($isValid) ?></div>
					</div>
					<!-- Later functionality -->
					<!-- style="border-bottom: solid 1px #476b6b; -->
					<div class="form-item form-item-sm">
						<div class="label">&nbsp;</div>
						<div class="input">
							<input type="submit" id="btnSubmit" name="btnSubmit" value="Log In">
						</div>	
					</div>
				</form>
				<div class="form-item form-item-sm">
					<div class="label">&nbsp;</div>
					<div class="input">
						<ul>
							<li><a href="/create_user.php">Create a Student Account?</a></li><br>
							<li><a href="/create_teacher.php">Create a Teacher Account?</a></li><br>
							<!-- Later functionality -->
							<!-- <li><a href="#">Forgot Password?</a></li><br>
							<li><a href="#">Forgot Email/Username?</a></li><br> -->
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- remove this div for user pages -->
		<div id="content-right" class="aside right-main">
			<div class="content">
				<!-- insert content -->
			</div>
		</div>
	</div>

<?php 
	require_once("includes/footer.inc.php");
?>
